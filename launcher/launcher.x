'
'
' ####################
' #####  PROLOG  #####
' ####################

' A simple launcher app
'
PROGRAM	"launcher"
VERSION	"0.0.0.1"
'
	IMPORT "xst_s.lib"
	IMPORT "xsx_s.lib"
	IMPORT "kernel32"
	IMPORT "gdi32"
	IMPORT "user32"
	IMPORT "shell32"
	IMPORT "msvcrt"
'

$$STR_FILE = 10
$$STR_PARAMS = 11

DECLARE FUNCTION Entry()
DECLARE FUNCTION RegisterClasses ()
DECLARE FUNCTION MessageLoop ()
DECLARE FUNCTION WndProc (hwnd, msg, wParam, lParam)
DECLARE FUNCTION DoLaunch()

FUNCTION Entry()
	SHARED	hInst
	SHARED hSplash
	hInst = GetModuleHandleW(0)		'can't live without this

	IFZ DoLaunch() THEN
		RETURN 1
	ENDIF

	hSplash = LoadImageA(hInst, 101, $$IMAGE_BITMAP, 0, 0, 0)
	RegisterClasses()
	#winMain = CreateWindowExA (0, &"Launcher", &"Hello English...", $$WS_POPUP|$$WS_BORDER, 0, 0, 300, 300, 0, 0, hInst, 0)
	IFZ #winMain THEN
		MessageBoxA(#winMain, &"Create window failed", &"Error", $$MB_OK)
	ENDIF
	XstCenterWindow (#winMain)
	ShowWindow (#winMain, $$SW_SHOWNORMAL)
	MessageLoop()
	
	RETURN 0
END FUNCTION
'
' #########################
' #####  MessageLoop  #####
' #########################
'
'
'
FUNCTION MessageLoop ()
	MSG msg

' main message loop

	DO																			' the message loop
		ret = GetMessageA (&msg, 0, 0, 0)	' retrieve next message from queue

		SELECT CASE ret
			CASE  0 : RETURN msg.wParam					' WM_QUIT message
			CASE -1 : RETURN $$TRUE							' error
			CASE ELSE:
				hwnd = GetActiveWindow ()
				IF (!IsWindow (hwnd)) || (!IsDialogMessageA (hwnd, &msg)) THEN		' send only non-dialog messages
  				DispatchMessageA (&msg)					' send message to window callback function WndProc()
				END IF
		END SELECT
	LOOP
END FUNCTION

FUNCTION RegisterClasses ()
	SHARED	hInst
	
	WNDCLASS wc
	
	'to register a window class, we fill in a WNDCLASS structure then pass it to the API
  wc.style						= $$CS_HREDRAW|$$CS_OWNDC|$$CS_VREDRAW
  wc.lpfnWndProc			= &WndProc()
  wc.cbClsExtra				= 0
  wc.cbWndExtra				= 0
  wc.hInstance				= hInst
  wc.hIcon						= 0
  wc.hCursor					= LoadCursorA (0, $$IDC_ARROW)
  wc.hbrBackground		= $$COLOR_BTNFACE + 1
  wc.lpszMenuName			= 0
  wc.lpszClassName		= &"Launcher"
	IFZ RegisterClassA (&wc) THEN
		MessageBoxA(#winMain, &"Register window failed", &"Error", $$MB_OK)
	ENDIF
END FUNCTION
'
' #########################
' #####  MenuWndProc  #####
' #########################
'
'
'
FUNCTION WndProc (hwnd, msg, wParam, lParam)
	SHARED hInst
	SHARED hSplash
	
	PAINTSTRUCT ps
	
	SELECT CASE msg
		CASE $$WM_CREATE
			SetTimer(hwnd, 1, 1000 * 5, 0)
		CASE $$WM_TIMER
			DestroyWindow(hwnd)
		CASE $$WM_PAINT
			hDC = BeginPaint (hwnd, &ps)
				hMem = CreateCompatibleDC (0)
				SelectObject (hMem, hSplash)
				BitBlt (hDC, 0, 0, 300, 300, hMem, 0, 0, $$SRCCOPY)
				DeleteDC (hMem)
			EndPaint (hwnd, &ps)
		CASE $$WM_DESTROY
			'perform any necassary cleanup
			PostQuitMessage(0)
		CASE ELSE
			RETURN DefWindowProcA (hwnd, msg, wParam, lParam)
	END SELECT
	RETURN 0
END FUNCTION

FUNCTION DoLaunch()
	SHARED hInst
	
	file$ = SPACE$(4096)
	IFZ LoadStringW(hInst, $$STR_FILE, &file$, 2048) THEN
		MessageBoxA(0, &"Launcher misconfigured, cannot find file string resource", &"Error", $$MB_OK | $$MB_ICONSTOP)
		RETURN 0
	END IF
	
	params$ = SPACE$(4096)
	IFZ LoadStringW(hInst, $$STR_PARAMS, &params$, 2048) THEN
		MessageBoxA(0, &"Launcher misconfigured, cannot find parameter string resource", &"Error", $$MB_OK | $$MB_ICONSTOP)
		RETURN 0
	END IF
	
	cdir$ = SPACE$(GetCurrentDirectoryW(0, 0) * 2)
	GetCurrentDirectoryW(LEN(cdir$)/2, &cdir$)
	
	openVerb$ =  XstAnsiToUnicode$("open")
	IF ShellExecuteW(0, &openVerb$, &file$, &params$, &cdir$, $$SW_SHOW) < 32 THEN
		code = GetLastError()
		title$ = XstAnsiToUnicode$("Error")
		pMessage = 0
		IFZ FormatMessageW($$FORMAT_MESSAGE_ALLOCATE_BUFFER | $$FORMAT_MESSAGE_FROM_SYSTEM, 0, code, 0, &pMessage, 0, 0) THEN
			msg$ = XstAnsiToUnicode$("Unknown error")
			MessageBoxW(0, &msg$, &error$, $$MB_OK | $$MB_ICONSTOP)
		ELSE
			MessageBoxW(0, pMessage, &error$, $$MB_OK | $$MB_ICONSTOP)
		END IF
		
		RETURN 0
	END IF
	
	RETURN 1
END FUNCTION