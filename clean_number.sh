#!/bin/bash

base=${1%.*}

echo "${base}.png"
convert "$1" -resize 200% "${base}_l.png"
composite -gravity Center "${base}_l.png" "number_template.png" "${base}_standard.png"
rm "${base}_l.png"

