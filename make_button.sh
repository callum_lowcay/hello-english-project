#!/bin/bash

base=${1%.*}

if [ -e "${base}t.png" ]
then
	echo "${base}.png"
	convert "$1" -resize 500x500 "${base}_small.png"
	composite -gravity Center "${base}_small.png" "blank.png" "${base}_button.png"
	composite -gravity North -geometry +0+88 "${base}_button.png" "buttonback.PNG" "${base}_button.png"
	composite -gravity North -geometry +0+660 "${base}t.png" "${base}_button.png" "${base}_button.png"
	rm "${base}_small.png"
	rm "${base}.png"
	rm "${base}t.png"
fi

