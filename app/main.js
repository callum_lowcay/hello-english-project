const {app, BrowserWindow} = require('electron');

var mainWindow = null;

app.on('window-all-closed', function() {
	// On OS X it is common for applications and their menu bar
	// to stay active until the user quits explicitly with Cmd + Q
	if (process.platform != 'darwin') {
		app.quit();
	}
});

app.on('ready', function() {
	// TODO: check for multiple monitors and try to spawn on the external
	// monitor.
	mainWindow = new BrowserWindow({audoHideMenuBar : true, useContentSize : true});

	mainWindow.loadURL('file://' + __dirname + '/assets/index.html');
	mainWindow.setMenuBarVisibility(false);
	mainWindow.setFullScreen(true);

	//mainWindow.webContents.openDevTools();

	global.muteState = {"mute":false};

	mainWindow.on('closed', function() {
		mainWindow = null;
	});
});

