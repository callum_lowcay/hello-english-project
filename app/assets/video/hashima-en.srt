﻿1
00:00:07,640 --> 00:00:09,240
We are on Hashima.

2
00:00:09,240 --> 00:00:11,640
 It's a World Heritage Site.

3
00:00:11,640 --> 00:00:14,480
Its shape is like a battleship.

4
00:00:14,900 --> 00:00:16,880
Many people lived here.

5
00:00:16,880 --> 00:00:19,540
Now, no one lives here.

6
00:00:19,540 --> 00:00:22,520
It's important for Japan's history.

