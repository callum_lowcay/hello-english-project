﻿1
00:00:07,380 --> 00:00:11,260
今日はキーワードゲームをやってみましょう。

2
00:00:11,260 --> 00:00:14,880
最初のキーワードは、「pencil」です。

3
00:00:14,880 --> 00:00:19,040
キーワードが聞こえたら、
消しゴムをとってください。

4
00:00:19,040 --> 00:00:22,460
早く消しゴムを取った方の勝ちです。

5
00:00:22,460 --> 00:00:25,300
それでは、始めましょう。

6
00:00:25,300 --> 00:00:27,880
Scissors.

7
00:00:27,880 --> 00:00:30,220
Eraser.

8
00:00:30,220 --> 00:00:32,240
Pen.

9
00:00:32,240 --> 00:00:34,080
Pencil.

10
00:00:34,840 --> 00:00:36,060
よくできました。

11
00:00:36,060 --> 00:00:38,320
それでは、もう一度やってみましょう。

