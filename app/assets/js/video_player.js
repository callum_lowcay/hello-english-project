var subtitles = [];

var defaultSubtitlesPriority = null;

// priority is used to rank preferred subtitles.  Use null if these subtitles
// should never be selected by default.  Lower numbers have higher priority.
function check_subtitles(url, name, priority) {
	$.ajax(url)
		.done(function (xhr, st) {
			if (defaultSubtitlesPriority === null || defaultSubtitlesPriority > priority) {
				$("#"+name).click();
				defaultSubtitlesPriority = priority;
			}
		})
		.fail(function (xhr, st, err) {
			$("#"+name).hide();
			$("label[for="+name+"]").hide();
		});
}

function parse_subtitles(url) {
	$.ajax(url).done(function (data) {
		const lines = data.split("\n")
			.map(function(s) {return s.trim();});

		subtitles = [];
		while (lines.length > 0) {
			if (lines[0] === "") {
				lines.shift();
				continue;
			}

			const n = lines.shift();
			const ts = parse_time_range(lines.shift());
			const sublines = [];
			while (lines.length > 0 && lines[0] !== "") {
				sublines.push(lines.shift());
			}
			lines.shift();

			subtitles.push({t:ts[0], d:sublines});
			subtitles.push({t:ts[1], d:null});
		}
	});
}

function parse_time_range(rawT) {
	return rawT.split("-->").map(function(s) {return parse_timecode(s.trim());});
}

function parse_timecode(tc) {
	var parts = tc.split(":");
	return parts[2].replace(",", ".") + (parts[1] * 60) + (parts[0] * 3600);
}

var subshowing = null;
var currentSubtitle = null;
function do_subtitle(time, subtitleBox) {
	var sublines = null;
	var x = null;
	var newsub = null;
	for (var i = 0; i < subtitles.length; i++) {
		if (time < subtitles[i].t) {
			newsub = subtitles[i].t;
			sublines = x == null? null : x.d;
			break;
		}
		x = subtitles[i];
	}

	if (newsub != subshowing) {
		if (sublines == null || sublines.length == 0) {
			subtitleBox.hide();
		} else {
			var sub = sublines[0];
			for (var i = 1; i < sublines.length; i++) {
				sub = sub + "<br>" + sublines[i];
			}

			subtitleBox.html("<div><p>" + sub + "</p></div>");
			subtitleBox.show();
		}
		subshowing = newsub;
	}
}

