const {remote} = require('electron');

var w = 0;
var h = 0;

var selectWordHooks = []
var selectWordAudioHooks = []

var global_nohide = false;
var global_noword = false;

function setNoHide(v) {
	global_nohide = v;
}

function setNoWord(v) {
	global_noword = v;
}

startupHooks = []
function doOnStartup(f) {
	startupHooks.push(f);
}

const muteState = remote.getGlobal("muteState");

var wordSelected = false;
var dialogMode = false;

$(document).ready(function() {
	autoResize();

	$("body").attr("ondragstart", "return false;");
	$("body").attr("ondrop", "return false;");

	$(".word").each(function (i) {
		var ji = $(this);
		ji.click(function () {selectWord(ji);});
	});

	if (muteState.mute) {
		$("#nav_mute").attr("checked", "checked");
		muteAll(true);
	}
	
	$("#nav_mute").change(function () {
		const checked = $("#nav_mute")[0].checked;
		muteState.mute = checked === true;
		muteAll(checked);
	});

	$("#nav_dialog").change(function () {
		const checked = $("#nav_dialog")[0].checked;
		dialogMode = checked === true;
		if (wordSelected) {
			if (dialogMode) {
				stopAllAudio();
				sayDialog($("#dialogResponse"));
			} else {
				stopAllAudio();
				sayDialog($("#selectedWord"));
			}
		}
	});

	$(".dialog").click(function () {sayDialog($(this));});

	$("#nav_close").click(function () {window.close();});

	// There seems to be a race condition with the resizing, so wait a few millis
	// before executing the starup hooks.
	setTimeout(function() {
		while (startupHooks.length > 0) {
			startupHooks.shift()();
		}
	}, 300);
});

function muteAll(doMute) {
	$("audio").each(function (index) {
		this.muted = doMute;
	});
}

function autoResize() {
	var w1 = $(window).width();
	var h1 = $(window).height();

	if (w !== w1 || h !== h1) {
		w = w1
		h = h1
		scalePage(w1, h1);
	}

	window.setTimeout(autoResize, 100);
}

var sw = 1;
var sh = 1;
var ox = 0;
var oy = 0;

function inverseTransform(p) {
	return {
		x:(p.x - ox) / sw,
		y:(p.y - oy) / sh
	};
}

function scalePage(w, h) {
	sw = w / 1920.0;
	sh = h / 1080.0;
	var s = 0;
	ox = 0;
	oy = 0;
	if (sw < sh) {
		s = sw;
		oy = (h - Math.floor(s * 1080.0)) / 2;
	} else {
		s = sh;
		ox = (w - Math.floor(s * 1920.0)) / 2;
	}

	$(".container").css("transform", "translate("+ox+"px, "+oy+"px) scale("+s+", "+s+")");
}

function sayDialog(jqDialogElem) {
	sayDialogCont(jqDialogElem, function() {});
}

var audioCancelFunctions = {};

function registerAudioCancelFunction(id, f) {
	audioCancelFunctions[id] = f;
}

function stopAudio(audio) {
	if (!audio) return;

	audio.pause();
	if (audioCancelFunctions[audio.id]) {
		audio.onended = audioCancelFunctions[audio.id];
	}
	audio.dispatchEvent(new Event('ended'));
	audio.currentTime = 0;
}

function stopAllAudioExcept(id) {
	const responseAudio = $("#responseAudio").get(0);
	const selectedWordAudio = $("#selectedWordAudio").get(0);
	const questionAudio = $("#questionAudio").get(0);
	if (id !== "responseAudio") stopAudio(responseAudio);
	if (id !== "selectedWordAudio") stopAudio(selectedWordAudio);
	if (id !== "questionAudio") stopAudio(questionAudio);
}

function stopAllAudio() {
	stopAllAudioExcept(null);
}

function sayDialogCont(jqDialogElem, cont) {
	const audioId = "#" + jqDialogElem.attr("data-audio");
	const audio = $(audioId).get(0);
	if (audio === undefined) return;
	if (!$(audioId + " source").attr("src")) return;

	audio.pause();
	stopAllAudioExcept(audio.id);

	if (jqDialogElem.hasClass("question")) {
		showQuestion();
	} else if (jqDialogElem.hasClass("response")) {
		showResponse();
	}

	const saidBy = $("#" + jqDialogElem.attr("data-said-by"));
	if (!saidBy.attr("data-original-src")) {
		saidBy.attr("data-original-src", saidBy.attr("src"));
	}

	const speakingSrc = saidBy.attr("data-speaking-src");
	if (speakingSrc !== 'undefined' && speakingSrc !== false && !muteState.mute) {
		saidBy.attr("src", speakingSrc);
	}

	audio.onended = function() {
		saidBy.attr("src", saidBy.attr("data-original-src"));
		audio.onended = null;
		cont();
	}
	audio.play();
}

function showQuestion() {
	if (!global_nohide) {
		$("#dialogResponse").css("display", "none");
		$("#selectedWord").css("display", "none");
	}
	$("#dialogQuestion").css("display", "block");
}

function showResponse() {
	if (!global_nohide) {
		$("#dialogQuestion").css("display", "none");
	}
	$("#dialogResponse").css("display", "block");
	$("#selectedWord").css("display", "block");
}

// get the name of the dialog file for a button file
function getDialog(filename) {
	return filename.replace(/\.[^/.]+$/, "_d.png");
}

// get the name of the big image file for a button file
function getImage(filename) {
	return filename.replace(/\.[^/.]+$/, "_i.png");
}

function selectWord(word) {
	const x = word.css("left");
	const y = word.css("top");
	const gh = $("#glow").height();
	const bh = word.height();
	const gw = $("#glow").width();
	const bw = word.width();
	const gyoff = (gh - bh) / 2;
	const gxoff = (gw - bw) / 2;
	const xx = x.match(/\d+/)[0] - gxoff;
	const yy = y.match(/\d+/)[0] - gyoff;

	$("#glow")
		.css({"top":yy+"px", "left":xx+"px", "display":"block", "opacity":0})
		.animate({"opacity":1}, "fast");

	$("#dialogResponse").attr("src", getDialog(word.attr("src")));
	$("#selectedWord").attr("src", getImage(word.attr("src")));
	showResponse();

	for (var i = 0; i < selectWordHooks.length; i++) {
		selectWordHooks[i](word);
	}

	const audioBase = word.attr("data-audio");
	$("#responseAudioSrc").attr("src", "audio/" + audioBase + "_r.mp3");
	if (!global_noword) {
		$("#selectedWordAudioSrc").attr("src", "audio/" + audioBase + "_w.mp3");
	}

	const responseAudio = $("#responseAudio").get(0);
	const selectedWordAudio = $("#selectedWordAudio").get(0);

	if (responseAudio !== undefined) responseAudio.load();
	if (selectedWordAudio !== undefined) selectedWordAudio.load();

	const saidBy = word.attr("data-said-by");
	if (saidBy) {
		$("#selectedWord").attr("data-said-by", saidBy);
		$("#dialogResponse").attr("data-said-by", saidBy);
	}

	wordSelected = true;

	if (global_noword || dialogMode) {
		sayDialog($("#dialogResponse"));
	} else {
		sayDialogCont($("#selectedWord"), function () {
			for (var i = 0; i < selectWordAudioHooks.length; i++) {
				selectWordAudioHooks[i]();
			}
		});
	}
}

// Add a function to execute after selecting a word
function afterSelectWord(f) {
	if (typeof f !== "function") throw "Not a function";
	selectWordHooks.push(f);
}

function afterSelectWordAudio(f) {
	if (typeof f !== "function") throw "Not a function";
	selectWordAudioHooks.push(f);
}

