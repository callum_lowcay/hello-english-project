var currentN = 0;

afterSelectWord(function(jqword) {
	const n = /[0-9]+/.exec(jqword.attr("src"))[0];
	currentN = n;
	for (var i = 0; i < stars.length; i++) {
		const star = "#star" + stars[i];
		if (i < n) {
			$(star).css("display", "block");
		} else {
			$(star).css("display", "none");
		}
	}
	$(".star").css({"transform":"none"});
});

registerAudioCancelFunction("selectedWordAudio", function(id) {
	$(".star").css({"transform":"none"});
	$("#selectedWordAudioSrc").attr("src", "audio/Helen/numbers/" + currentN + "_w.mp3");
	const selectedWordAudio = $("#selectedWordAudio").get(0);
	if (selectedWordAudio !== undefined) selectedWordAudio.load();
	$("#helen").attr("src", $("#helen").attr("data-original-src"));
	selectedWordAudio.onended = null;
});

for (var i = 1; i < (stars.length + 1); i++) {
	addTwinkling($("#star" + i));
}
startTwinkle();

$(document).ready(function() {
	$(".star").click(countStars);
});

function countStars() {
	stopAllAudio();
	const target = currentN;
	const starsList = stars.slice(0, target).sort(function(x, y) {return x - y;});
	const nextStar = function(i) {
		return function() {
			if (i > 0) $("#star" + (starsList[i - 1])).css({"transform":"none"});
			if (i >= target || currentN !== target) return;

			const n = starsList[i];
			$("#star" + n).css({"transform":"scale(2,2)"});

			$("#selectedWordAudioSrc").attr("src", "audio/Helen/numbers/" + (i + 1) + "_w.mp3");
			const selectedWordAudio = $("#selectedWordAudio").get(0);
			if (selectedWordAudio !== undefined) selectedWordAudio.load();
			sayDialogCont($("#selectedWord"), nextStar(i + 1));
		};
	}
	nextStar(0)();
}

