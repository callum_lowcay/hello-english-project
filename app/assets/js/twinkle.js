function startTwinkle() {
	window.setInterval(twinkle, 160);
}

var twinkleElems = []

function addTwinkling(jelem) {
	twinkleElems.push(jelem);
}

function twinkle() {
	twinkleElems.forEach(function(x) {
		x.css("opacity", Math.floor((Math.random() * 20) + 80) / 100);
	});
}

